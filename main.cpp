﻿#include <iostream>
#include <string>

using namespace std;

enum class UnitType {
    Elf,
    Human,
    Goblin,
    Orc,
    Gnome
};

class GameUnit {
private:
    UnitType type;
    int health;
    int age;
    int level;
    string name;
    int distanceTraveled;

public:
    GameUnit(UnitType unitType) : type(unitType), health((unitType == UnitType::Orc) ? 90 : 100), age(0), level(1), name(""), distanceTraveled(0) {}

    GameUnit(UnitType unitType, int unitAge) : type(unitType), health((unitType == UnitType::Orc) ? 90 : 100), age(unitAge), level(1), name(""), distanceTraveled(0) {}

    void hit() {
        int damage = rand() % 10 + 1;
        double healthLost = health * (damage / 100.0);
        health -= static_cast<int>(healthLost);
        if (health <= 0) {
            resetUnit();
        }
    }

    void printInfo() {
        cout << "Unit Type: ";
        switch (type) {
        case UnitType::Elf:
            cout << "Elf";
            break;
        case UnitType::Human:
            cout << "Human";
            break;
        case UnitType::Goblin:
            cout << "Goblin";
            break;
        case UnitType::Orc:
            cout << "Orc";
            break;
        case UnitType::Gnome:
            cout << "Gnome";
            break;
        }
        cout << "\nHealth: " << health << "\nAge: " << age << "\nLevel: " << level << "\nName: " << name << "\nDistance Traveled: " << distanceTraveled << " km\n";
    }

    bool isAlive() {
        return health > 10;
    }

    void walk(int distance) {
        distanceTraveled += distance;
        if (distanceTraveled >= 1000) {
            level++;
            distanceTraveled -= 1000;
        }
    }

private:
    void resetUnit() {
        health = 0;
        age = 0;
        level = 1;
        name = "";
        distanceTraveled = 0;
    }
};

int main() {
    GameUnit elfUnit(UnitType::Elf, 50);
    elfUnit.printInfo();

    GameUnit orcUnit(UnitType::Orc);
    orcUnit.printInfo();

    while (elfUnit.isAlive()) {
        elfUnit.hit();
        elfUnit.printInfo();
    }

    return 0;
}



/*

	Определить класс "Игровой юнит" со следующими условиями

	Перечень полей:
	- Тип
	- Здоровье
	- Возраст
	- Уровень
	- Имя
	- пройдено км

	У юнита усть тип: Эльф, Человек, Гоблин, Орк, Гном (перечисление)
	При создании юнита нужно всегда указывать его тип. (конструктор 1)

	Конструктор 2 позволяет задавать ещё т возраст юнита (вместе с типом)

	При создании юнита всех, кроме Орка, здоровте по-умолчанию 100, иначе 90;
	На здоровте можно влиять только внутри класса, задать его иначе нельзя;

	Методы:
	Удар() void (При ударе теряет от 1 до 10% здоровья, если здоровте в результате удара стало 0, то информация о юните обнуляется)
	Вывод информации() void
	ПроверкаЖивЛиЮнит() логический
	Идти(кол-во км) void (при прохождении каждой 1000 км, уровень поднимается на 1)

	*/